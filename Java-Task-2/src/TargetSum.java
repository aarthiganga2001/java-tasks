import java.util.*;

public class TargetSum {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("n: ");
        int n = sc.nextInt();

        int[] array = new int[n];
        System.out.println("Enter the array elements: ");
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }
        System.out.println("Target: ");
        int target = sc.nextInt();

        HashSet<Integer> map = new HashSet<>();
        for (int i = 0; i < n; i++) {
            int element = target - array[i];
            if (!map.contains(element)) {
                map.add(array[i]);
            } else {
                System.out.println("Found[" + element + "," + array[i] + "]");
                return;
            }
        }
        System.out.println("Not found");

    }
}

