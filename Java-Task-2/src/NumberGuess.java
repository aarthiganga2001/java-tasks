import java.util.*;

public class NumberGuess {

    static int i;

    public void calculate(String c, String expression, int randomNumber)
    {
        StringTokenizer tokenizer = new StringTokenizer(expression);
        String[] str=new String[2];
        str[0]=tokenizer.nextToken(c);
     //   System.out.println(str[0]);
        str[1]=tokenizer.nextToken("=");
      //  System.out.println(str[1]);
        String val=tokenizer.nextToken();
    //    System.out.println(val);
        StringTokenizer tokenizer2 = new StringTokenizer(str[1],c);
        String val2=tokenizer2.nextToken();
    //    System.out.println(val2);
        int num1 = Integer.parseInt(str[0]);
        int num2=Integer.parseInt(val2);
        switch (c)
        {
            case "+":
                if((num1+num2)==Integer.parseInt(val))
                {
                    if(Integer.parseInt(val)==randomNumber) {
                        System.out.println("Your guess was right !");
                        System.out.println(randomNumber);
                        System.exit(0);
                    }
                    else {
                        System.out.println("Your guess was wrong!");
                        break;
                    }
                }
                else {
                    i--;
                    System.out.println("Invalid Expression");
                    break;
                }
            case "-":
                if((num1-num2)==Integer.parseInt(val)) {
                    if (Integer.parseInt(val) == randomNumber) {
                        System.out.println("Your guess was right !");
                        System.out.println(randomNumber);
                        System.exit(0);
                    } else
                    {
                        System.out.println("Your guess was wrong!");
                    break;
                }
                }
                else {
                    i--;
                    System.out.println("Invalid Expression");
                    break;
                }
            case "*":
                if((num1*num2)==Integer.parseInt(val))
                {
                    if(Integer.parseInt(val)==randomNumber)
                    {
                        System.out.println("Your guess was right !");
                        System.out.println(randomNumber);
                        System.exit(0);
                    }
                    else {
                        System.out.println("Your guess was wrong!");
                        break;
                    }
                }
                else {
                    i--;
                    System.out.println("Invalid Expression");
                    break;
                }
            case "/":
                if((num1/num2)==Integer.parseInt(val)) {
                    if (Integer.parseInt(val) == randomNumber) {
                        System.out.println("Your guess was right !");
                        System.out.println(randomNumber);
                        System.exit(0);
                    } else
                    {
                        System.out.println("Your guess was wrong!");
                    break;
                }
                }
                else {
                    i--;
                    System.out.println("Invalid Expression");
                    break;
                }
            case "%":
                if((num1%num2)==Integer.parseInt(val))
                {
                    if(Integer.parseInt(val)==randomNumber)
                    {
                        System.out.println("Your guess was right !");
                        System.out.println(randomNumber);
                        System.exit(0);
                    }
                    else {
                        System.out.println("Your guess was wrong!");
                        break;
                    }
                }
                else {
                    i--;
                    System.out.println("Invalid Expression");
                    break;
                }
        }
    }
    public void comparison(String c, String expression, int randomNumber)
    {
        StringTokenizer tokenizer = new StringTokenizer(expression);
        String[] str=new String[2];
        str[0]=tokenizer.nextToken(c);
        str[1]=tokenizer.nextToken();
      try {
          int val=Integer.parseInt(str[1]);
          i--;
          System.out.println("Invalid expression");
      }
      catch(Exception e)
      {
           int num1 = Integer.parseInt(str[0]);
           if (c.equals("<")) {
               if (num1 < randomNumber) {
                   System.out.println("Your guess is right!");
               }
               else
                   System.out.println("Your guess is wrong!");
           } else if(c.equals((">"))) {
               if (num1 > randomNumber) {
                   System.out.println("Your guess is right!");
               }
               else
                   System.out.println("Your guess is wrong!");
           }
           else
               System.out.println("Your guess is wrong!");
           }
       }


    public static void main(String[] args) {

        NumberGuess number = new NumberGuess();

        String ch = null;

        Scanner sc = new Scanner(System.in);
        Random random=new Random();
        int randomNumber = random.nextInt(100)+1;
        System.out.println("\t\t\tGuess the number between 1-100\t\t\t");
        for (i = 0; i < 10; i++) {
            System.out.println("Chance " + (i + 1) + ": ");
            String expression = sc.next();

            if (expression.contains("+")) {
                ch = "+";
                number.calculate(ch,expression,randomNumber);

            }

            else if (expression.contains("-"))
            {
                ch = "-";
                number.calculate(ch,expression,randomNumber);

            }

            else if (expression.contains("*"))
            {
                ch = "*";
                number.calculate(ch,expression,randomNumber);

            }
            else if (expression.contains("/"))
            {
                ch = "/";
                number.calculate(ch,expression,randomNumber);

            }
            else if (expression.contains("%"))
            {
                ch = "%";
                number.calculate(ch,expression,randomNumber);

            }
            else if (expression.contains("<"))
            {
                ch = "<";
                number.comparison(ch,expression,randomNumber);

            }

            else if (expression.contains(">"))
            {
                ch = ">";
                number.comparison(ch,expression,randomNumber);

            }

        }
        System.out.println(randomNumber);
    }
}
