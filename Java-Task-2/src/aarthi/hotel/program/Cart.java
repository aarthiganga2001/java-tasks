package aarthi.hotel.program;

public class Cart {

    String category, food;
    int quantity;
    Float price, totalPrice;

    public Cart(String category, String food, int quantity, Float price, Float totalPrice) {
        this.category = category;
        this.food = food;
        this.quantity = quantity;
        this.price = price;
        this.totalPrice = totalPrice;
    }
}
