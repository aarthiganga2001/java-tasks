package aarthi.hotel.program;

import java.io.IOException;
import java.util.*;

public class Admin {

    HashMap<String, Float> type = new HashMap<>();

    public void updateMenu() throws IOException {

        Hotel h = new Hotel();
        Scanner sc = new Scanner(System.in);

        int choice;
        System.out.println("1.Add\t2.Update price\t3.Remove\t4.Back");
        choice = sc.nextInt();


        if (choice == 1) {
            System.out.println("1.Add food to existing Category\t2.Add new Category\t3.Back");
            int ch = sc.nextInt();

            if (ch == 1) {
                System.out.println("Enter the Category of food: ");
                String userCategory = sc.next();

                if (!Hotel.menu.containsKey(userCategory)) {
                    System.out.println("Invalid Category");
                    return;
                }
                System.out.println("Enter the food name: ");
                String foodName = sc.next();
                System.out.println("Enter the price: ");
                Float price = sc.nextFloat();


                HashMap<String, Float> initial = new HashMap<>(Hotel.menu.get(userCategory));
                initial.put(foodName, price);

                h.addToMenuCard(userCategory, initial);

            } else if (ch == 2) {
                System.out.println("Enter the Category of food: ");
                String userCategory = sc.next();

                if (Hotel.menu.containsKey(userCategory)) {
                    System.out.println("Invalid Category");
                    return;
                }

                HashMap<String, Float> initial = new HashMap<>();

                Hotel.category.add(userCategory);
                System.out.println("Enter the no. of items to be added in " + userCategory + " Category: ");
                int n = sc.nextInt();
                for (int j = 0; j < n; j++) {
                    sc.nextLine();
                    System.out.println("Enter the item name along with it's price: ");
                    String item = sc.nextLine();
                    Float price = sc.nextFloat();
                    initial.put(item, price);

                }
                h.addToMenuCard(userCategory, initial);
            } else {
                updateMenu();
            }

        } else if (choice == 2) {
            System.out.println("Enter the Category of food: ");
            String userCategory = sc.next();

            if (!Hotel.menu.containsKey(userCategory)) {
                System.out.println("Invalid Category");
                return;
            }


            System.out.println("Food name: ");
            String foodName = sc.next();

            System.out.println("New price: ");
            Float newPrice = sc.nextFloat();


            HashMap<String, Float> initial = new HashMap<>(Hotel.menu.get(userCategory));
            if (initial.containsKey(foodName)) {
                initial.replace(foodName, initial.get(foodName), newPrice);
                h.addToMenuCard(userCategory, initial);
            } else {
                System.out.println("Food name not found");
            }
        } else if (choice == 3) {
            System.out.println("Enter the Category of food: ");
            String userCategory = sc.next();

            if (!Hotel.menu.containsKey(userCategory)) {
                System.out.println("Invalid Category");
                return;
            }


            System.out.println("Food name: ");
            String foodName = sc.next();

            HashMap<String, Float> initial = new HashMap<>(Hotel.menu.get(userCategory));
            if (initial.containsKey(foodName)) {
                initial.remove(foodName);
                h.addToMenuCard(userCategory, initial);
            } else {
                System.out.println("Food name not found");
            }
        } else {
            admin();
        }
    }


    public void displayMenu() {
        System.out.println("---------\t\t\tMenu Card\t\t\t---------");
        for (int i = 0; i < Hotel.menu.size(); i++) {

            type.putAll(Hotel.menu.get(Hotel.category.get(i)));
            System.out.println("\n" + Hotel.category.get(i));
            System.out.println("-------------------------");
            type.forEach((key, value) -> System.out.println(key + " " + value));
            type.clear();
        }

    }

    public void addMenu() {
        Hotel h = new Hotel();

        Scanner sc = new Scanner(System.in);

        if (Hotel.menu.size() == 0) {
            System.out.println("Enter the no. of Category to be added: ");
            int numOfCategory = sc.nextInt();

            for (int i = 0; i < numOfCategory; i++) {
                HashMap<String, Float> initial = new HashMap<>();
                System.out.println("Enter the Category name: ");
                String categoryName = sc.next();
                Hotel.category.add(categoryName);
                System.out.println("Enter the no. of items to be added in " + categoryName + " Category: ");
                int n = sc.nextInt();
                for (int j = 0; j < n; j++) {
                    sc.nextLine();
                    System.out.println("Enter the item name: ");
                    String item = sc.nextLine();
                    System.out.println("Price: ");
                    Float price = sc.nextFloat();
                    initial.put(item, price);
                }
                h.addToMenuCard(categoryName, initial);
            }

        } else {
            System.out.println("Menu already added");
        }
    }

    public void adminLogin() throws IOException {
        Scanner sc = new Scanner(System.in);

        String adminName = "admin";
        String adminPassword = "12345";
        System.out.println("Enter admin name: ");
        String aname = sc.next();
        System.out.println("Enter admin password: ");
        String apass = sc.next();

        if (!(aname.equals(adminName) && apass.equals(adminPassword))) {
            System.out.println("Invalid credentials");
            return;
        }
        admin();
    }

    public void admin() throws IOException {
        Scanner sc = new Scanner(System.in);

        int choice;
        while (true) {
            System.out.println("1.Add menu\t2.Update menu\t3.Display menu\t4.Back");
            choice = sc.nextInt();

            if (choice == 1) {
                addMenu();
            } else if (choice == 2) {
                updateMenu();
            } else if (choice == 3) {
                displayMenu();
            } else {
                Hotel h = new Hotel();
                h.init();
            }
        }
    }
}
