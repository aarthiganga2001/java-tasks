package aarthi.hotel.program;

import java.io.*;
import java.util.*;

public class Hotel {

    static HashMap<String, HashMap<String, Float>> menu = new HashMap<>();
    static ArrayList<String> category = new ArrayList<>();

    static ArrayList<User> users = new ArrayList<>();

    public String userLogin() throws IOException {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter user name: ");
        String uname = sc.next();
        System.out.println("Enter user password: ");
        String upass = sc.next();
        int flag = 0;
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).userPassword.equals(upass)) {
                flag = 1;
                System.out.println("Login successful");
                break;
            }

        }
        if (flag != 1) {
            users.add(new User(uname, upass));
            System.out.println("Login successful");
        }
        return uname;
    }

    public void viewMenu() {
        HashMap<String, Float> type = new HashMap<>();
        Scanner sc = new Scanner(System.in);

        System.out.println("---------\t\t\tMenu Card\t\t\t---------");
        for (int i = 0; i < Hotel.menu.size(); i++) {
            type.putAll(Hotel.menu.get(Hotel.category.get(i)));
            System.out.println(Hotel.category.get(i));
            System.out.println("-------------------------");
            type.forEach((key, value) -> System.out.println(key + " " + value));

            type.clear();
        }
    }

    public void addToMenuCard(String categoryName, HashMap<String, Float> initial) {
        menu.put(categoryName, initial);
    }

    public void init() throws IOException {
        Scanner sc = new Scanner(System.in);
        int choice;
        while (true) {
            System.out.println("1.Admin\t2.User\t3.Exit");
            choice = sc.nextInt();
            if (choice == 1) {
                Admin adm = new Admin();
                adm.adminLogin();
            } else if (choice == 2) {
                String user = userLogin();
                Users us = new Users();
                us.users(user);
            } else {
                System.exit(0);
            }
        }
    }

    public static void main(String[] args) throws IOException {

        System.out.println("\t----\t----\tHotel Application\t----\t----\t");
        Hotel htl = new Hotel();
        htl.init();
    }
}
