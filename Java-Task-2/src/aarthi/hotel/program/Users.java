package aarthi.hotel.program;

import java.io.*;
import java.util.*;

public class Users {


    File file = new File("UserHistory.txt");
    ArrayList<Cart> cart = new ArrayList<>();

    String addCategory, addFood;
    int quant;
    Float amount, totalAmount;
    static int num = 1;

    public void addToCart(String uname) throws IOException {



        Scanner sc = new Scanner(System.in);
        HashMap<String, Float> initial = new HashMap<>();

        while (true) {

            FileWriter fwrite = new FileWriter(file, true);

            System.out.println("Do you need to add items to your cart(Y/N): ");
            String op = sc.next();

            sc.nextLine();
            if (op.equalsIgnoreCase("y")) {
                System.out.println("Enter Category: ");
                addCategory = sc.nextLine();

                if (!Hotel.menu.containsKey(addCategory)) {
                    System.out.println("Invalid Category");
                    return;
                }

                System.out.println("Enter food name: ");
                addFood = sc.nextLine();


                amount = null;


                initial.putAll(Hotel.menu.get(addCategory));

                if (initial.containsKey(addFood)) {
                    amount = initial.get(addFood);
                } else {
                    System.out.println("Invalid food name");
                    return;
                }


                System.out.println("Enter quantity: ");
                quant = sc.nextInt();

                totalAmount = amount * quant;

                cart.add(new Cart(addCategory, addFood, quant, amount, totalAmount));
                fwrite.write(num + " " + uname + " ");


                num++;
                fwrite.write(addCategory + " " + addFood + " " + quant + " " + amount + " " + totalAmount + "\n");
                fwrite.close();

            } else {
                break;
            }


        }
    }

    public void myCart() {
        Float amount = (float) 0;
        if (cart.size() != 0) {
            System.out.println("My items in cart...");
            for (int i = 0; i < cart.size(); i++) {
                System.out.println((i + 1) + "\t" + cart.get(i).category + "\t" + cart.get(i).food + "\t" + cart.get(i).quantity + "\t" + cart.get(i).price + "\t" + cart.get(i).totalPrice);
                amount += cart.get(i).totalPrice;
            }
            System.out.println("======================================");
            System.out.println("Your total bill amount is Rs." + amount);
        } else {
            System.out.println("Cart is empty");
        }
    }


    public void users(String user) throws IOException {
        int choice;
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("1.View and add food to cart\t2.Items in cart\t3.Back");
            choice = sc.nextInt();

            if (choice == 1) {
                Hotel h = new Hotel();
                h.viewMenu();
                addToCart(user);
            } else if (choice == 2) {
                myCart();
            } else {
                Hotel ho = new Hotel();
                ho.init();
            }

        }
    }
}
