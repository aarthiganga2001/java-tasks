package aarthi.com.hotel;

public class Items {


    String hotelName,category, food;
    int quantity;
    Float price, totalPrice;

    public Items(String hotelName,String category, String food, int quantity, Float price, Float totalPrice) {
        this.hotelName=hotelName;
        this.category = category;
        this.food = food;
        this.quantity = quantity;
        this.price = price;
        this.totalPrice = totalPrice;
    }
}
