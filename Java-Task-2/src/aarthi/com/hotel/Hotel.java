package aarthi.com.hotel;

import java.io.*;
import java.util.*;

public class Hotel {

    HashMap<String, HashMap<String, Float>> menu = new HashMap<>();
    static ArrayList<Hotel> hotels=new ArrayList<>();
    static HashMap<Integer,String> hotelNames=new HashMap<>();

    HashMap<String, Float> type = new HashMap<>();
    ArrayList<String> category=new ArrayList<>();
    static ArrayList<User> users = new ArrayList<>();

   // ArrayList<String> userNames=new ArrayList<>();
    static String userName;

    Hotel()
    {
        HashMap<String, Float> category1 = new HashMap<>();
        category1.put("chicken rice", 150F);
        category1.put("veg fried rice", 100F);
        category1.put("egg fried rice", 50F);
        category.add("rice");
        menu.put("rice",category1);
        HashMap<String, Float> category2 = new HashMap<>();
        category2.put("dal", 80F);
        category2.put("masala curry", 150F);
        category2.put("panneer gravy", 200F);
        category.add("curry");
        menu.put("curry",category2);
        HashMap<String, Float> category3 = new HashMap<>();
        category3.put("lemon", 10F);
        category3.put("soda", 20F);
        category.add("juices");
        menu.put("juices",category3);

    }

    public static void viewOrders() throws FileNotFoundException {

        if(Users.file.canRead()) {
            Scanner sc = new Scanner(Users.file);
            System.out.println("No.\tUser Name\tHotel Name\tCategory\tFood Name\tQuantity\tPrice\tTotal Price");
            while (sc.hasNext()) {
                System.out.println(sc.nextLine());
            }
        }
        else
        {
            System.out.println("File is empty");
        }
    }
    public static Object chooseHotelByAdmin() throws IOException {
        int hotelNum = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Available restaurants: ");
        hotelNames.forEach((key, value) -> System.out.println(key + " " + value));
        System.out.println("Choose the hotel: ");
        try {
            hotelNum = sc.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid input");
            adminRoles();

        }
        System.out.println(hotelNum);
        return hotels.get(hotelNum - 1);
    }

    public static Object chooseHotelByUser() throws IOException {
        int hotelNum = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Available restaurants: ");
        hotelNames.forEach((key, value) -> System.out.println(key + " " + value));
        System.out.println("Choose the hotel: ");
        try {
            hotelNum = sc.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid input");
            Users.userRoles(userName);

        }
        System.out.println(hotelNum);
        return hotels.get(hotelNum - 1);
    }


    public static void updateMenu() throws IOException {

        Hotel obj= (Hotel) chooseHotelByAdmin();

        Scanner sc = new Scanner(System.in);

        try {
            int choice;
            System.out.println("1.Add\t2.Update price\t3.Remove\t4.Back");
            choice = sc.nextInt();


            if (choice == 1) {
                System.out.println("1.Add food to existing Category\t2.Add new Category\t3.Back");
                int ch = sc.nextInt();

                if (ch == 1) {
                    System.out.println("Enter the Category of food: ");
                    String userCategory = sc.next();

                    if (!obj.menu.containsKey(userCategory)) {
                        System.out.println("Invalid Category");
                        return;
                    }
                    System.out.println("Enter the food name: ");
                    String foodName = sc.next();
                    System.out.println("Enter the price: ");
                    Float price = sc.nextFloat();


                    HashMap<String, Float> initial = new HashMap<>(obj.menu.get(userCategory));
                    initial.put(foodName, price);

                    obj.addToMenuCard(userCategory, initial, obj);

                } else if (ch == 2) {
                    System.out.println("Enter the Category of food: ");
                    String userCategory = sc.next();

                    if (obj.menu.containsKey(userCategory)) {
                        System.out.println("Invalid Category");
                        return;
                    }

                    HashMap<String, Float> initial = new HashMap<>();

                    obj.category.add(userCategory);
                    System.out.println("Enter the no. of items to be added in " + userCategory + " Category: ");
                    int n = sc.nextInt();
                    for (int j = 0; j < n; j++) {
                        sc.nextLine();
                        System.out.println("Enter the item name along with it's price: ");
                        String item = sc.nextLine();
                        Float price = sc.nextFloat();
                        initial.put(item, price);

                    }
                    obj.addToMenuCard(userCategory, initial, obj);
                } else {
                    updateMenu();
                }

            } else if (choice == 2) {
                System.out.println("Enter the Category of food: ");
                String userCategory = sc.next();

                if (!obj.menu.containsKey(userCategory)) {
                    System.out.println("Invalid Category");
                    return;
                }


                System.out.println("Food name: ");
                String foodName = sc.next();

                System.out.println("New price: ");
                Float newPrice = sc.nextFloat();


                HashMap<String, Float> initial = new HashMap<>(obj.menu.get(userCategory));
                if (initial.containsKey(foodName)) {
                    initial.replace(foodName, initial.get(foodName), newPrice);
                    obj.addToMenuCard(userCategory, initial, obj);
                } else {
                    System.out.println("Food name not found");
                }
            } else if (choice == 3) {
                System.out.println("Enter the Category of food: ");
                String userCategory = sc.next();

                if (!obj.menu.containsKey(userCategory)) {
                    System.out.println("Invalid Category");
                    return;
                }


                System.out.println("Food name: ");
                String foodName = sc.next();

                HashMap<String, Float> initial = new HashMap<>(obj.menu.get(userCategory));
                if (initial.containsKey(foodName)) {
                    initial.remove(foodName);
                    obj.addToMenuCard(userCategory, initial, obj);
                } else {
                    System.out.println("Food name not found");
                }
            } else {
                adminRoles();
            }
        }
        catch (Exception e)
        {
            System.out.println("\nInvalid input");
            updateMenu();
        }
    }

        public static void viewMenu(Hotel obj) {

            int index = hotels.indexOf(obj);

            System.out.println("---------\t\t\tMenu Card of Hotel: " + hotelNames.get(index + 1) + "\t\t\t---------");
            for (int i = 0; i < obj.menu.size(); i++) {

                obj.type.putAll(obj.menu.get(obj.category.get(i)));
                System.out.println("\n" + obj.category.get(i));
                System.out.println("-------------------------");
                obj.type.forEach((key, value) -> System.out.println(key + " " + value));
                obj.type.clear();
            }

        }

public static void addHotels()
{
    try {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the no. of hotels to be added: ");
        int numOfHotels = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < numOfHotels; i++) {
            System.out.println("Enter hotel " + (i + 1) + " name: ");
            hotelNames.put((i + 1), sc.nextLine());
            hotels.add(new Hotel());

        }
    }
    catch (Exception e)
    {
        System.out.println("Invalid input");
        addHotels();
    }

}
    public static void adminRoles() throws IOException {
        Scanner sc = new Scanner(System.in);

        int choice = 0;
        while (true) {
            System.out.println("\n1.Add hotels\t2.Update menu\t3.View menu\t4.View Orders\t5.Back");
            try {
                choice = sc.nextInt();
            }
            catch (Exception e)
            {
                System.out.println("Invalid input");
                adminRoles();
            }

            if (choice == 1) {
                addHotels();
            } else if (choice == 2) {
                updateMenu();
            } else if (choice == 3) {
                if(!hotels.isEmpty()) {
                    Hotel obj = (Hotel) Hotel.chooseHotelByAdmin();
                    viewMenu(obj);
                }
                else {
                    System.out.println("\nPlease add hotels :(");
                    System.exit(0);
                }
            } else if (choice==4) {
                viewOrders();
            } else {
                init();
            }
        }
    }

    public static void userSignUp()
    {
        try {
            Scanner sc = new Scanner(System.in);

            System.out.println("Enter user name: ");
            String uname = sc.next();
            System.out.println("Enter user password: ");
            String upass = sc.next();
            users.add(new User(uname, upass));
            userName = uname;
        }
        catch (Exception e)
        {
            System.out.println("Invalid input");
            userSignUp();
        }
    }
    public static void userLogin(){
        try {
            Scanner sc = new Scanner(System.in);

            System.out.println("Enter user name: ");
            String uname = sc.next();
            System.out.println("Enter user password: ");
            String upass = sc.next();
            int flag = 0;
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).userPassword.equals(upass)) {
                    flag = 1;
                    System.out.println("Login successful");
                    userName = uname;
                    break;
                }

            }
            if (flag != 1) {
                System.out.println("Invalid user");
                System.exit(0);
            }
        }
        catch (Exception e)
        {
            System.out.println("Invalid input");
            userLogin();
        }
    }

    public void addToMenuCard(String categoryName, HashMap<String, Float> initial, Hotel obj) {
        obj.menu.put(categoryName, initial);
    }

    public static void init() throws IOException {
        String adminName = "admin",adminPassword = "12345";
        Scanner sc = new Scanner(System.in);
        int choice = 0;
        while (true) {
            System.out.println("\n1.Admin\t2.User\t3.Exit");
            try {
                choice = sc.nextInt();
            }
            catch (Exception e)
            {
                System.out.println("Invalid input");
                init();
            }
            if (choice == 1) {
                String aname = null;
                String apass = null;
                try {
                    System.out.println("Enter admin name: ");
                    aname = sc.next();
                    System.out.println("Enter admin password: ");
                    apass = sc.next();
                } catch (Exception e) {
                    System.out.println("Invalid input");
                    init();
                }
                if (!(aname.equals(adminName) && apass.equals(adminPassword))) {
                    System.out.println("Invalid credentials");
                    System.exit(0);
                } else {
                    System.out.println("Admin login successful");
                }
                adminRoles();
            } else if (choice == 2) {
                int n = 0;
                System.out.println("1.Existing User\t2.New User");
                try {
                    n = sc.nextInt();
                }
                catch (Exception e)
                {
                    System.out.println("Invalid input");
                    init();
                }
                if(n==1)
                    userLogin();
                else
                    userSignUp();

                Users.userRoles(userName);

            } else {
                System.exit(0);
            }
        }
    }

    public static void main(String[] args) throws IOException {

        System.out.println("\t----\t----\tFood Ordering Application\t----\t----\t");
        Users.file.delete();
        init();




    }
}
