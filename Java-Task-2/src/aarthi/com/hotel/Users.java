package aarthi.com.hotel;

import java.io.*;
import java.util.*;

public class Users {


    static File file = new File("UserHistory.txt");
    static ArrayList<Items> cart = new ArrayList<>();
    
    static int num = 1;

    public static void addToCart(String uname,Hotel obj) throws IOException {

        String category, foodName;
        int quant;
        Float amount, totalAmount;

        Scanner sc = new Scanner(System.in);
        HashMap<String, Float> initial = new HashMap<>();

        while (true) {

            try {
                FileWriter fwrite = new FileWriter(file, true);

                System.out.println("\nDo you need to add items to your cart(Y/N): ");
                String op = sc.next();

                sc.nextLine();
                if (op.equalsIgnoreCase("y")) {
                    System.out.println("Enter Category: ");
                    category = sc.nextLine();

                    if (!obj.menu.containsKey(category)) {
                        System.out.println("Invalid Category");
                        return;
                    }

                    System.out.println("Enter food name: ");
                    foodName = sc.nextLine();


                    amount = null;


                    initial.putAll(obj.menu.get(category));

                    if (initial.containsKey(foodName)) {
                        amount = initial.get(foodName);
                    } else {
                        System.out.println("Invalid food name");
                        return;
                    }


                    System.out.println("Enter quantity: ");
                    quant = sc.nextInt();

                    totalAmount = amount * quant;

                    int index = Hotel.hotels.indexOf(obj);

                    cart.add(new Items(Hotel.hotelNames.get(index + 1), category, foodName, quant, amount, totalAmount));
                    fwrite.write(num + "\t" + uname + "\t\t");


                    num++;
                    fwrite.write(Hotel.hotelNames.get(index + 1) + "\t\t" + category + "\t\t" + foodName + "\t\t" + quant + "\t\t" + amount + "\t\t" + totalAmount + "\n");
                    fwrite.close();

                } else {
                    break;
                }
            }
            catch (FileNotFoundException e)
            {
                System.out.println("File does not exist");
                addToCart(uname,obj);
            }
            catch (InputMismatchException e)
            {
                System.out.println("Invalid input");
                addToCart(uname,obj);
            }

        }
    }

    public static void billSection()
    {
        Float amount = (float) 0;

        for (int i = 0; i < cart.size(); i++) {
            amount += cart.get(i).totalPrice;
        }

        System.out.println("Your total bill amount is Rs." + amount);
        cart.clear();

    }
    public static void myCart() {
        if (!cart.isEmpty()) {
            System.out.println("\nMy items in cart...");
            for (int i = 0; i < cart.size(); i++) {
                System.out.println((i + 1) + "\t" + cart.get(i).hotelName+" "+cart.get(i).category + "\t" + cart.get(i).food + "\t" + cart.get(i).quantity + "\t" + cart.get(i).price + "\t" + cart.get(i).totalPrice);
            }
        } else {
            System.out.println("Cart is empty");
        }
    }


    public static void userRoles(String user) throws IOException {
        int choice = 0;
        Scanner sc = new Scanner(System.in);
        
        while (true) {
            System.out.println("\n1.View menu and add items to cart\t2.Items in cart\t3.Bill section\t4.Back");
            try {
                choice = sc.nextInt();
            }
            catch (Exception e)
            {
                System.out.println("Invalid input");
                userRoles(user);
            }

            if (choice == 1) {
                if(!Hotel.hotels.isEmpty()) {
                    Hotel obj = (Hotel) Hotel.chooseHotelByUser();
                    Hotel.viewMenu(obj);
                    addToCart(user,obj);
                }
                else {
                    System.out.println("Sorry, No hotels are added yet :(");
                    System.exit(0);
                }

            } else if (choice == 2) {
                myCart();
            } else if (choice==3) {
                billSection();
            } else {
                cart.clear();
                Hotel.init();
            }

        }
    }
}
