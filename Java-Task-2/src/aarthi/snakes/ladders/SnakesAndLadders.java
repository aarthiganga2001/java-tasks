package aarthi.snakes.ladders;

import java.util.*;

class SnakesAndLadders {

    static HashMap<Integer, Integer> snakesLadders = new HashMap<>();

    static {

        //snakes
        snakesLadders.put(32, 10);
        snakesLadders.put(36, 6);
        snakesLadders.put(48, 26);
        snakesLadders.put(62, 18);
        snakesLadders.put(88, 24);
        snakesLadders.put(96, 56);
        snakesLadders.put(98, 78);

        //ladders
        snakesLadders.put(4, 14);
        snakesLadders.put(8, 10);
        snakesLadders.put(1, 38);
        snakesLadders.put(21, 42);
        snakesLadders.put(28, 76);
        snakesLadders.put(50, 67);
        snakesLadders.put(71, 92);
        snakesLadders.put(80, 99);

    }

    ArrayList<Player> players = new ArrayList<>();


    public int markPosition(int actualPlayerPosition, int diceValue) {
        if ((actualPlayerPosition + diceValue) > 100) {
            int p = (actualPlayerPosition + diceValue) - 100;
            actualPlayerPosition = 100-p;
        } else {
            actualPlayerPosition += diceValue;
        }

        if (snakesLadders.containsKey(actualPlayerPosition)) {
            if (snakesLadders.get(actualPlayerPosition) > actualPlayerPosition) {
                System.out.println("Went up through ladder");
            } else {
                System.out.println("Got bit by a snake");
            }
            actualPlayerPosition = snakesLadders.get(actualPlayerPosition);

        }
        return actualPlayerPosition;
    }

    public void checkWinner(int pos, String person) {
        if (pos == 100) {
            System.out.println("\n" + person + "'s position: " + (pos));
            System.out.println("\n" + person + " is the winner");
            System.exit(0);
        }
    }

    public int rollDice(String person) {
        Scanner sc = new Scanner(System.in);
        int randomNumber = 0;
        System.out.println("\n" + person + " roll the dice...");
        sc.nextLine();
        Random random=new Random();
        randomNumber = random.nextInt(6)+1;
        System.out.println("Dice: " + randomNumber);
        return randomNumber;
    }

    public void addPlayers() {
        int initialPosition = 0;

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the number of players: ");
        int numOfPlayers = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < numOfPlayers; i++) {
            System.out.println("Enter player " + (i + 1) + " name: ");
            String name = sc.nextLine();

            players.add(new Player(name, initialPosition));


        }

    }


    public void startGame() {
        int actualPlayerPosition = 0;
        int diceValue;
        System.out.println("Let's Begin...");
        while (true) {
            for (int i = 0; i < players.size(); i++) {

                String person = players.get(i).playerName;

                actualPlayerPosition = players.get(i).playerPosition;

                checkWinner(actualPlayerPosition, person);

                System.out.println("\n" + person + "'s position: " + (actualPlayerPosition));

                diceValue = rollDice(person);


                if (actualPlayerPosition == 0) {
                    if (diceValue == 1) {
                        actualPlayerPosition = markPosition(actualPlayerPosition, diceValue);

                        System.out.println("\n" + person + "'s position: " + (actualPlayerPosition));
                        System.out.println("\n" + person + " is in the game :)");
                        players.get(i).setPlayerPosition(actualPlayerPosition);

                    } else {
                        while (diceValue == 6) {
                            diceValue = rollDice(person);

                        }
                        if (diceValue == 1) {

                            actualPlayerPosition = markPosition(actualPlayerPosition, diceValue);

                            System.out.println("\n" + person + "'s position: " + (actualPlayerPosition));
                            System.out.println("\n" + person + " is in the game :)");
                            players.get(i).setPlayerPosition(actualPlayerPosition);


                        }
                    }

                } else {
                    while (diceValue == 6) {
                        actualPlayerPosition = markPosition(actualPlayerPosition, diceValue);
                        checkWinner(actualPlayerPosition, person);

                        System.out.println("\n" + person + "'s position: " + (actualPlayerPosition));
                        diceValue = rollDice(person);
                    }

                    actualPlayerPosition = markPosition(actualPlayerPosition, diceValue);
                    checkWinner(actualPlayerPosition, person);


                    System.out.println("\n" + person + "'s position: " + (actualPlayerPosition));

                    players.get(i).setPlayerPosition(actualPlayerPosition);

                }


            }


        }

    }
}
