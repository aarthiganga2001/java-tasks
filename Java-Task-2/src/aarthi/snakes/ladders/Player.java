package aarthi.snakes.ladders;

class Player {

    String playerName;
    int playerPosition;

    Player(String playerName, int playerPosition) {
        this.playerName = playerName;
        this.playerPosition = playerPosition;
    }

    public void setPlayerPosition(int pos) {
        this.playerPosition = pos;
    }

}
