package aarthi.snakes.ladders;

import java.util.*;

public class Game {

    public static void main(String[] args) {

        System.out.println("\t\t\tSnakes and Ladders Game\t\t\t");
        SnakesAndLadders sl = new SnakesAndLadders();
        sl.addPlayers();
        sl.startGame();

    }
}