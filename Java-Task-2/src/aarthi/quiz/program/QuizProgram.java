package aarthi.quiz.program;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class QuizProgram implements Runnable{

    ArrayList<Question> questionSet = new ArrayList<>();
    ArrayList<Participant> participants = new ArrayList<>();

    int i=0;
    String user;
    static int answer;
    File file = new File("Results.txt");
    QuizProgram()
    {
        String[] optionSet1={"Aug 11","Sep 20","Aug 15","July 5"};
        questionSet.add(new Question("When is independence day celebrated in India?",10,3,optionSet1));
        String[] optionSet2={"Bangalore","New Delhi","Chennai"};
        questionSet.add(new Question("Capital of India?",10,2,optionSet2));
        String[]  optionSet3={"True","False"};
        questionSet.add(new Question("Is Chennai a metropolitan city?",5,1,optionSet3));

    }
    public void checkResults() throws IOException {


        if (participants.size() == 0) {
            System.out.println("No quiz took place");
            return;
        }

        Scanner sc=new Scanner(file);

        while (sc.hasNextLine())
        {
            System.out.println(sc.nextLine());
        }

    }


    @Override
    public void run() {
        answer=0;
        Scanner sc=new Scanner(System.in);
        System.out.println("Choose the right option: ");
        try {
            answer = sc.nextInt();
        }
        catch (Exception e)
        {
            System.out.println("Invalid input");
            run();
        }

    }
    public void startQuiz() throws InterruptedException, IOException {



        Scanner sc=new Scanner(System.in);


        System.out.println("\nDear Participant, Welcome to the quiz program :)");

        System.out.print("\nEnter your name: ");
        user = sc.next();

        System.out.println("\nGeneral Instruction: All questions are mandatory. Each question has a time limit, so try to answer before time exceeds");
        System.out.println("\nThere are total " + questionSet.size() + " questions.");
        System.out.println("\nAll the best...Lets Begin...\n");

        Thread.sleep(3000);

        // task.scheduledExecutionTime();
        int point = 0;



        for(i=0;i<questionSet.size();i++) {

            System.out.print("\nQuestion " + (i + 1) + ": ");
            System.out.print(questionSet.get(i).question);

            System.out.println("\nOptions: ");

            for (int j = 0; j < questionSet.get(i).options.length; j++)
                System.out.println((j + 1) + " " + questionSet.get(i).options[j]);

            System.out.println("Time limit: " + questionSet.get(i).time + " secs");

            //while(true) {

            Thread thread = new Thread(new QuizProgram());
            thread.start();

            long start = System.currentTimeMillis();

            while (!thread.isAlive()) ;

            while ((System.currentTimeMillis() - start < questionSet.get(i).time * 1000) && thread.isAlive()) {

            }

            if (thread.isAlive()) {
                System.out.println("\n<>Time elapsed<>");
            } else if (questionSet.get(i).correctOption==answer) {
                System.out.println("\nYour answer was correct!!! +1 point");
                point++;
            } else {
                System.out.println("\nWrong one !!");
            }

        }
        System.out.println("\nQuiz Finished, Thanks for your participation :)");
        System.out.println("\nYour Result:");
        System.out.println("\nUser: " + user + " Points: " + point + "/" + questionSet.size());

        LocalDateTime myDateObj=LocalDateTime.now();
        DateTimeFormatter myFormatObj=DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedDate=myDateObj.format(myFormatObj);
        participants.add(new Participant(user, point,formattedDate));

        Comparator<Participant> compPoints = Comparator.comparing(Participant::getPoint).reversed().thenComparing(Participant::getUser);
        Collections.sort(participants, compPoints);


        FileWriter fwrite = new FileWriter(file);

        fwrite.write("\t\t\tResult Board\t\t\t\n");
        fwrite.write("---------------------------------\n");
        for (int i = 0; i < participants.size(); i++) {
            fwrite.write((i + 1) + "\t" + participants.get(i).user + "\t" + participants.get(i).point +"\t"+ participants.get(i).formattedDate+ "\n");
        }
        fwrite.close();



    }


    public void addQuestions() {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of questions to be added: ");
        int numOfQues = 0;
        try {
            numOfQues = sc.nextInt();
        }
        catch (Exception e)
        {
            System.out.println("Invalid input");
            addQuestions();
        }


        for (int i = 0; i < numOfQues; i++) {
            try {
                sc.nextLine();
                System.out.print("Question " + (i + 1) + ": ");
                String ques = sc.nextLine();

                System.out.println("Enter the no. of options to be added: ");
                int numOfOptions = sc.nextInt();
                String[] options = new String[numOfOptions];

                sc.nextLine();
                for (int j = 0; j < numOfOptions; j++) {
                    System.out.print("Enter option " + (j + 1) + " : ");
                    options[j] = sc.nextLine();
                }
                System.out.print("Enter the time limit in secs: ");
                int sec = sc.nextInt();

                System.out.print("Enter the correct option number: ");
                int crtOp = sc.nextInt();

                System.out.println();
                questionSet.add(new Question(ques, sec, crtOp, options));
            }
            catch (Exception e)
            {
                System.out.println("Invalid input");
                i--;
                continue;
            }
        }
    }

    public void init() throws IOException, InterruptedException {
        Scanner sc = new Scanner(System.in);
        int choice = 0;
        while (true) {
            System.out.println("\n1.Interviewer\t2.Participant\t3.Results\t4.Exit");
            try {
                choice = sc.nextInt();
            }
            catch (Exception e)
            {
                System.out.println("Invalid input");
                init();
            }
            if (choice == 1) {
                addQuestions();
            } else if (choice == 2) {
                startQuiz();
            } else if (choice == 3) {
                checkResults();
            } else {
                System.exit(0);
            }

        }

    }

    public static void main(String[] args) throws IOException, InterruptedException {

        System.out.println("\t\t\tQuiz Program\t\t\t");
        QuizProgram qp = new QuizProgram();
        qp.init();

    }

}
