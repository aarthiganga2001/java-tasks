package aarthi.quiz.program;

class Participant {

    String user;
    int point;

    Participant(String user, int point) {
        this.user = user;
        this.point = point;
    }

    int getPoint() {
        return point;
    }

    String getUser() {
        return user;
    }

}

