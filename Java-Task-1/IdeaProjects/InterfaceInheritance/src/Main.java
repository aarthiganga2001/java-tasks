import java.util.*;
import java.lang.Math;

interface Shape {
    void getArea();

    void getPerimeter();

    void getDetails();


}

class Square implements Shape {
    Scanner sc = new Scanner(System.in);
    int side;

    @Override
    public void getArea() {
        System.out.print("Enter the side of the square:");
        side = sc.nextInt();
        int result = side * side;
        System.out.println("Area of square is " + result);
    }

    @Override
    public void getPerimeter() {
        int perimeter = 4 * side;
        System.out.println("Perimeter of square is " + perimeter);
    }

    @Override
    public void getDetails() {
        System.out.println("Side of square is " + side);
    }
}

class Circle implements Shape {
    float radius;
    Scanner sc = new Scanner(System.in);

    @Override
    public void getArea() {
        System.out.print("\nEnter the radius of the circle:");
        radius = sc.nextFloat();

        float result = (float) (Math.PI * radius * radius);
        System.out.println("Area of circle is " + result);
    }

    @Override
    public void getPerimeter() {
        float perimeter = (float) (2 * Math.PI * radius);
        System.out.println("Perimeter of circle is " + perimeter);

    }

    @Override
    public void getDetails() {
        System.out.println("Radius of circle is " + radius);

    }
}

class Triangle implements Shape {
    int side1, side2, base, height;
    Scanner sc = new Scanner(System.in);

    @Override
    public void getArea() {
        System.out.print("\nEnter the side1, side2, base and height of the triangle:");
        side1 = sc.nextInt();
        side2 = sc.nextInt();
        base = sc.nextInt();
        height = sc.nextInt();
        int result = (base * height) / 2;
        System.out.println("Area of triangle is " + result);


    }

    @Override
    public void getPerimeter() {
        int perimeter = side1 + side2 + base;
        System.out.println("Perimeter of triangle is " + perimeter);
    }

    @Override
    public void getDetails() {
        System.out.println("Side 1 of triangle is " + side1);
        System.out.println("Side 2 of triangle is " + side2);
        System.out.println("Base of the triangle is " + base);
        System.out.println("Height of the triangle is " + height);
    }
}


public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Square square = new Square();

        square.getArea();
        square.getPerimeter();
        square.getDetails();

        Circle circle = new Circle();
        circle.getArea();
        circle.getPerimeter();
        circle.getDetails();


        Triangle triangle = new Triangle();
        triangle.getArea();
        triangle.getPerimeter();
        triangle.getDetails();
    }


}

