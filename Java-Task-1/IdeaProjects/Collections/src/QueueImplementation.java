import java.util.*;

public class QueueImplementation {
    int[] Queue;
    int front, rear, limit;

    Scanner sc = new Scanner(System.in);

    QueueImplementation(int s) {
        front = rear = 0;
        limit = s;
        Queue = new int[s];
    }

    public void enqueue() {

        int element;
        if (rear < limit) {
            System.out.print("Element: ");
            element = sc.nextInt();
            Queue[rear] = element;
            rear++;
        } else {
            System.out.println("Queue is full");
        }

    }

    public void dequeue() {
        if (front != rear) {

            for (int i = 0; i < rear - 1; i++) {
                Queue[i] = Queue[i + 1];
            }
            rear--;

        } else {
            System.out.println("Queue is empty");
        }
    }

    public void print() {
        if (rear != 0) {
            for (int i = 0; i < rear; i++) {
                System.out.println(Queue[i] + " ");
            }
        } else {
            System.out.println("Queue is empty");
        }
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Queue size: ");
        int size = sc.nextInt();
        QueueImplementation queue = new QueueImplementation(size);
        while (true) {
            int choice;
            System.out.println("1.Enqueue\t2.Dequeue\t3.Print all elements\t4.Exit");
            choice = sc.nextInt();

            if (choice == 1) {
                queue.enqueue();
            } else if (choice == 2) {
                queue.dequeue();
            } else if (choice == 3) {
                queue.print();
            } else {
                System.exit(0);
            }

        }

    }
}
