import java.util.*;

public class ListInteger {
    static List<Integer> list = new ArrayList<>();

    public void sorting() {
        System.out.println("After Sorting:");
//        for (int i = 0; i < list.size(); i++) {
//            for (int j = i + 1; j < list.size(); j++) {
//                int num1 = list.get(i);
//                int num2 = list.get(j);
//                if (num1 > num2) {
//                    list.set(i, num2);
//                    list.set(j, num1);
//                }
//            }
//        }

        Collections.sort(list);
        System.out.println(list);
    }



    public void greaterSmaller() {
      //  System.out.println("Greater number: " + list.get(list.size() - 1));
        System.out.println("Greater number: " + Collections.max(list));
      //  System.out.println("Smaller number: " + list.get(0));
        System.out.println("Smaller number: " + Collections.min(list));

    }

    public static void main(String[] args) {

        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of list elements: ");
        n = sc.nextInt();
        System.out.println("Input the list elements:");
        for (int i = 0; i < n; i++) {
            list.add(sc.nextInt());
        }
        ListInteger li = new ListInteger();
        li.sorting();
        li.greaterSmaller();

    }
}

