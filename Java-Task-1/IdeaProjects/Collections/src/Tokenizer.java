import java.util.*;

public class Tokenizer {
    private String something = "some";
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the line of string: ");
        String str = sc.nextLine();
        StringTokenizer st = new StringTokenizer(str);
        TreeSet<String> tree = new TreeSet<>();
        while (st.hasMoreTokens()) {
            tree.add(st.nextToken());
        }
        System.out.println(tree);
        Tokenizer e = new Tokenizer();
        System.out.println(e);
    }
}
