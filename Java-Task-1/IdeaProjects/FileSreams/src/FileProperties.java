import java.util.*;
import java.io.*;

public class FileProperties {

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Input file name to check: ");
        String fileName = sc.next();

        File file = new File(fileName);
        File fi = new File("Newfile.txt");

        if (file.exists()) {
            System.out.println("File exists");
        } else {
            System.out.println("File doesn't exist");
            return;
        }

        if (file.canRead()) {
            System.out.println("File is readable");
        } else {
            System.out.println("File is not readable");
        }

        if (file.canWrite()) {
            System.out.println("File is writable");
        } else {
            System.out.println("File is not writable");
        }

        String fname = file.getName();
        String ext = "";
        for (int i = 0; i < fname.length(); i++) {
            if (fname.charAt(i) == '.') {
                ext = fname.substring(i + 1);
            }
        }
        System.out.println("File type is " + ext);
        System.out.println("File length in bytes is " + file.length());

        FileInputStream fis = new FileInputStream(file);
        FileOutputStream fos = new FileOutputStream(fi);
        int v = 0;
        while ((v = fis.read()) != -1) {
            fos.write((char) v);
        }

    }
}
