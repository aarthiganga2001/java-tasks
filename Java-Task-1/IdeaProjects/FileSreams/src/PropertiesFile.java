import java.io.*;
import java.util.*;

public class PropertiesFile implements Closeable {

    public static void main(String[] args) throws IOException {

        try (PropertiesFile pf = new PropertiesFile()){
            FileInputStream fp = new FileInputStream("info.properties");
            Properties prop = new Properties();

            prop.load(fp);

            Set s=prop.entrySet();

            Iterator itr=s.iterator();
            while (itr.hasNext())
            {
Map.Entry entry=(Map.Entry)itr.next();
                System.out.println(entry.getKey()+" "+entry.getValue());

}
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    @Override
    public void close() throws IOException {
        System.out.println("Close");
    }
}
