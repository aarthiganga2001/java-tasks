public class Exceptions {

    public static void main(String[] args) {

        try
        {
            int ans=10/0;
            System.out.println(ans);

            String name = null;
            System.out.println(name.concat("hello"));
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        catch (ArithmeticException e)
        {
            System.out.println(e);
        }
        catch (NullPointerException e)
        {
            System.out.println(e);
        }
    }
}
