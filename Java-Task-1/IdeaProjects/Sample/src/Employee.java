import java.util.*;
interface Measurable
{
    double getMeasure();
}

public class Employee
{
    private String name;
    private double salary;

    Employee()
    {

    }
    Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    String getName()
    {
        return name;
    }

    double getSalary()
    {
        return salary;
    }

    void Salary(Employee[] objects)
    {

        Comparator<Employee> compSalary=Comparator.comparing(Employee::getSalary).thenComparing(Employee::getName);

        Arrays.sort(objects,compSalary);

        System.out.println("\nSorted salary list breaking ties by name: ");
        for(int i=0;i<objects.length;i++)
        {
            System.out.println(objects[i].name+" "+objects[i].salary);
        }

        Comparator<Employee> compName=Comparator.comparing(Employee::getName).thenComparing(Employee::getSalary);

        Arrays.sort(objects,compName);

        System.out.println("\nSorted name list breaking ties by salary: ");
        for(int i=0;i<objects.length;i++)
        {
            System.out.println(objects[i].name+" "+objects[i].salary);
        }
    }

    Employee[] largest(Employee[] objects)
    {
        double max=0;
        int index=0;
        for(int i=0;i<objects.length;i++)
        {
            if(objects[i].salary>max)
            {
                max=objects[i].salary;
                index=i;
            }
        }
        System.out.println("\nPerson with highest salary is "+objects[index].name);

        return objects;

    }
    double average(Employee[] objects)
    {

        Measurable measure=()->{
            int sum=0;
            for (Employee obj:objects) {
                sum+=obj.salary;
            }
            return (sum)/(objects.length);
        };
        double avg= measure.getMeasure();
        return avg;
    }


    public static void main(String[] args) {

        Employee[] objects={new Employee("Ram",30000.00),
                new Employee("Sham",30000.00),
                new Employee("Suresh",32780.80),
                new Employee("Ram",27478.70),
                new Employee("Kishore",30000.0)};

        Employee emp=new Employee();
        System.out.println("\nAverage salary of array of employees is "+emp.average(objects));

        Employee emp2=new Employee();
        emp2.Salary(emp.largest(objects));



    }

}


