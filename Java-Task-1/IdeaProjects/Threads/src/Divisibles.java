import java.util.*;
import java.lang.*;

public class Divisibles implements Runnable{

        @Override
        public void run() {
            for(int i=1001;i<2000;i++)
            {
                if(i%7==0||i%11==0||i%13==0){
                    System.out.println(Thread.currentThread().getName()+" Priority-"+Thread.currentThread().getPriority()+" "+i);
                }
            }
        }

        public static void main(String[] args) throws InterruptedException {

            Runnable r=new Divisibles();
            Thread t1=new Thread(r);
            Thread t2=new Thread(r);
            Thread t3=new Thread(r);

            t1.setPriority(1);
            t2.setPriority(10);
            t3.setPriority(5);

            t1.start();
            t1.join();
            t2.start();
            t2.join();
            t3.start();
          //  t3.join();



        }


    }

