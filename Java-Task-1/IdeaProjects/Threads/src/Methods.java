import java.sql.SQLOutput;
import java.util.*;
import java.lang.*;

public class Methods implements Runnable {

    public static void main(String[] args) throws InterruptedException {

        Runnable r = new Methods();
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        Thread t3 = new Thread(r);

       System.out.println(Thread.currentThread().getName()+" "+Thread.currentThread().getState());

        Thread.sleep(1000);

        t1.start();
        t2.start();
        t3.start();
        t1.suspend();
        t3.suspend();
        t1.stop();

        t2.resume();
        t2.suspend();
        t2.stop();

        t3.suspend();
        t3.stop();
    }

    @Override
    public void run() {


        System.out.println(Thread.currentThread().getName() + " "+Thread.currentThread().getState());

    }
}
