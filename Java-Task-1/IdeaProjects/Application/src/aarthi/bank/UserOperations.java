package aarthi.bank;

import java.io.*;
import java.util.Scanner;
public class UserOperations {

    String accNum,Name;
    static Scanner sc = new Scanner(System.in);

    BankAccount bac = new BankAccount();

    File file2=new File("Transactions.txt");



    UserOperations(String accNum, String Name) throws IOException {
        this.accNum = accNum;
        this.Name=Name;
    }

    public void assignInitial() throws IOException {

        FileWriter fwrite2=new FileWriter(file2,true);
        System.out.println("Enter the initial amount: ");
        int initial = sc.nextInt();

        for (int i = 0; i < bac.bank.size(); i++) {
            System.out.println(accNum);
            System.out.println(bac.bank.get(i).accNo);
            if (bac.bank.get(i).accNo.equals(accNum)) {
                bac.bank.set(i, new BankAccount(bac.bank.get(i).name, accNum, bac.bank.get(i).accType, bac.bank.get(i).balance + initial));
                fwrite2.write("Initial amount assigned: Rs."+initial+"\t Balance: Rs."+bac.bank.get(i).balance+"\n");
                fwrite2.close();
                System.out.println("Assigned successfully :)");
                break;
            }
        }
    }

    public void depositAmount() throws IOException {
        FileWriter fwrite2=new FileWriter(file2,true);
        System.out.println("Enter the amount to deposit: ");
        int deposit = sc.nextInt();

        for (int i = 0; i < bac.bank.size(); i++) {
            if (bac.bank.get(i).accNo.equals(accNum)) {
                bac.bank.set(i, new BankAccount(bac.bank.get(i).name, accNum, bac.bank.get(i).accType, bac.bank.get(i).balance + deposit));
                fwrite2.write("Deposited: Rs."+deposit+"\t Balance: "+bac.bank.get(i).balance+"\n");
                fwrite2.close();
                System.out.println("Deposited successfully :)");
                break;
            }
        }
    }

    public void withdrawAmount() throws InterruptedException, IOException {

        FileWriter fwrite2=new FileWriter(file2,true);
        for (int i = 0; i < bac.bank.size(); i++) {
            if (bac.bank.get(i).accNo.equals(accNum)) {
                int bal = bac.bank.get(i).balance;
                System.out.println("Available balance is " + bal);
                System.out.println("Enter the amount to withdraw: ");
                int withdraw = sc.nextInt();
                if (bal > withdraw) {
                    bac.bank.set(i, new BankAccount(bac.bank.get(i).name, accNum, bac.bank.get(i).accType, bac.bank.get(i).balance - withdraw));
                    fwrite2.write("Withdrawn: Rs."+withdraw+"\t Balance: "+bac.bank.get(i).balance+"\n");
                    fwrite2.close();
                    System.out.println("Withdrawn successfully :)");
                    break;
                } else {
                    System.out.println("No sufficient balance");
                }
            }
        }
    }

        public void displayDetails ()
        {
            for (int i = 0; i < bac.bank.size(); i++) {
                if (bac.bank.get(i).accNo.equals(accNum)) {
                    System.out.println("Name: " + bac.bank.get(i).name);

                    int bal = bac.bank.get(i).balance;
                    System.out.println("Available balance is " + bal);
                }
            }
        }
        public void user () throws InterruptedException, IOException {

            FileWriter fwrite2=new FileWriter(file2,true);

            fwrite2.write("\n\t\t\tTransaction Details\t\t\t");
            fwrite2.write("\n\nAccount Holder: "+Name+"\t Account Number: "+accNum+"\n");
            fwrite2.write("--------------------------------------------------------\n");
            fwrite2.close();
            int choice;

            while (true) {
                System.out.println("1.Assign initial amount");
                System.out.println("2.Deposit amount");
                System.out.println("3.Withdraw amount");
                System.out.println("4.Display account details");
                System.out.println("5.Back");

                choice = sc.nextInt();

                switch (choice) {
                    case 1:
                        assignInitial();
                        break;
                    case 2:
                        depositAmount();
                        break;
                    case 3:
                        withdrawAmount();
                        break;
                    case 4:
                        displayDetails();
                        break;
                    case 5:
                        BankAccount ba = new BankAccount();
                        ba.initial();
                }
            }
        }
    }

