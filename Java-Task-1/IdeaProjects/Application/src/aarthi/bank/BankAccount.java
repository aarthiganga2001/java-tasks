package aarthi.bank;
import java.io.*;
import java.util.*;

public class BankAccount {

    File file = new File("AccountDetails.txt");

    String name,accNo,accType;
    int balance;
    static Scanner sc=new Scanner(System.in);
    static ArrayList<BankAccount> bank=new ArrayList<>();

    String adminName,adminPassword;

    BankAccount(String adminName,String adminPassword)
    {
        this.adminName=adminName;
        this.adminPassword=adminPassword;

        bank.add(new BankAccount("Aarthi","1000","Salary",0));
        bank.add(new BankAccount("Arun","1001","Salary",0));
        bank.add(new BankAccount("Amith","1002","Savings",0));
        bank.add(new BankAccount("Jana","1003","Savings",0));
        bank.add(new BankAccount("Jesh","1004","Savings",0));
        bank.add(new BankAccount("Aadhi","1005","Savings",0));
        bank.add(new BankAccount("Gokul","1006","Salary",0));
        bank.add(new BankAccount("Keerthi","1007","Savings",0));
        bank.add(new BankAccount("Nanthini","1008","Salary",0));
        bank.add(new BankAccount("Yoga","1010","Salary",0));

    }

    BankAccount()
    {

    }
    BankAccount(String name, String accNo, String accTYpe, int balance)
    {
        this.name=name;
        this.accNo=accNo;
        this.accType=accTYpe;
        this.balance=balance;
    }

    public void adminLogin() throws InterruptedException, IOException {

        System.out.println("Enter admin name: ");
        String aname=sc.next();
        System.out.println("Enter admin password: ");
        String apass=sc.next();

        if(adminName.equals(aname)&&adminPassword.equals(apass))
        {
            System.out.println("Admin login successful :)");

            if(file.length()==0) {
                System.out.println("Update customer details to the file");

                FileWriter fwrite = new FileWriter(file);
                fwrite.write("Customer Account Details\n");
                fwrite.write("------------------------\n");
                for (int i = 0; i < bank.size(); i++) {
                    fwrite.write(bank.get(i).name + " " + bank.get(i).accNo + " " + bank.get(i).accType + " " + bank.get(i).balance + "\n");

                }

                fwrite.close();

                Thread.sleep(5000);
                System.out.println("Updated successfully :)");
            }
            else {
                System.out.println("Details already uploaded");
            }




        }
        else
        {
            System.out.println("Invalid login");
            BankAccount bacc=new BankAccount();
            bacc.initial();
        }
    }

    public void userLogin() throws InterruptedException, IOException {


        System.out.println("Enter account number: ");
        String userAccNo=sc.next();

        for(int i=0;i<bank.size();i++)
        {
            if(bank.get(i).accNo.equals(userAccNo))
            {
                System.out.println("User login successful :)");

                UserOperations uo=new UserOperations(userAccNo,bank.get(i).name);
                uo.user();

            }
        }
            System.out.println("Invalid login");
            BankAccount b=new BankAccount();
            b.initial();



    }
    public void initial() throws InterruptedException, IOException {
        int choice;
        while(true) {
            System.out.println("1.Admin Login\t2.User Login\t3.Exit");
            choice = sc.nextInt();

            if(choice==1)
            {
                adminLogin();
            }
            else if(choice==2)
            {
                userLogin();
            }
            else
            {
                FileWriter fwrite3 = new FileWriter(file);
                fwrite3.write("Customer Account Details\n");
                fwrite3.write("------------------------\n");
                for (int i = 0; i < bank.size(); i++) {
                    fwrite3.write(bank.get(i).name + " " + bank.get(i).accNo + " " + bank.get(i).accType + " " + bank.get(i).balance + "\n");

                }

                fwrite3.close();
                System.exit(0);
            }
        }
    }
    public static void main(String[] args) throws InterruptedException, IOException {

        System.out.println("\t---\t---\tBank Application\t---\t---\t");
        BankAccount ba=new BankAccount("admin","12345");
        ba.initial();

    }
}
